# Demo: Keycloak KrakenD Microservices

Using Keycloak as the Identity Access Management service, OIDC JWT tokens are issued to the client application which then make authenticated requests through KrakenD API Gateway to a couple example microservices.

## High Level Detail

![Diagram of architecture](/docs/images/diagram.png)

## Getting Started

1. ```git clone git@gitlab.com:bensiewert/demo-microservices-with-keycloak-and-krakend.git```

2. ```docker-compose up --build```

3. Navigate to ```http://localhost:8080``` and login with sample credentials of ```username: admin``` and ```password: admin```. These are demo credentials and should not be used in production. This is the ```Keycloak``` admin login page.

    ![keycloak login page](/docs/images/keycloak-login.PNG)

4. In the Keycloak dashboard, hover over the "Master" Realm dropdown and click "Add realm". 

    ![Add new realm](/docs/images/keycloak-new-realm.png)

5. Import demo realm export. In the ```/config/keycloak``` folder is a ```real-export.json``` file we can import that is configured and ready to use for this demo.

    ![import realm json file](/docs/images/keycloak-import-realm-button.png)

6. After importing the realm from above, make sure the ```Name``` is ```myrealm``` and click ```Create```

    ![create imported realm](/docs/images/keycloak-create-realm.png)

7. In this demo project folder navigate to the ```client``` folder and start the Angular application with ```npm run start```, then navigate to ```http://localhost:4200``` when it is up and running. 

    ![open the angular application](/docs/images/angular-start.PNG)

8. In the Angular app, open up the browser dev-tools and go to the network tab then click the ```call microservice 1``` button. Notice a 401 unauthorized response.

    ![microservice 401](/docs/images/angular-microservice-401.png)

9. In the Keycloak app, create a realm application user. Click the ```Add user``` button. Then add a ```Username```, ```First Name```, and ```Last Name```. Ensure ```User Enabled``` is ```ON``` and ```Email Verified``` is ```ON```. Click Save. This is a manual way to add users to the system. TODO: create the logic for user signup/registration. 
    ![create real app user](/docs/images/keycloak-create-realm-appuser.png)

    ![add username and save](/docs/images/keycloak-create-realm-appuser-2.png)

10. Create a password for the newly created app user. Enter a password and ensure ```Temporary``` is ```OFF``` and click ```Set password```.

    ![add username and save](/docs/images/keycloak-create-realm-appuser-3.png)

11. In the Angular app, click the ```login``` button and note we are redirected to the ```Keycloak``` hosted login page to begin the auth code PKCE flow. This page style can be customized per the ```Keycloak``` docs [here](https://www.keycloak.org/docs/latest/server_development/#_themes). This is the ```Keycloak``` client login page to login with the realm we imported for our users. Enter the credentials you made for the app user in the previous step and you will be redirected back to the Angular client app.

    ![keycloak client login page](/docs/images/keycloak-login.PNG)

12. In the Angular app, after logging in with the Keycloak IAM service, you will be redirected to the Angular app where you are now authenticated and any API requests to the sample microservices will now be validated in the ```KrakenD``` API gateway. If you click the ```call microservice 1``` button you'll see that it will now return a success response with json.

    ![angular call microservice 1 success](/docs/images/angular-microservice-200.png)

## Technologies

1. [Keycloak](https://www.keycloak.org/) - "Open Source Identity and Access Management". A client is configured in this service to issue OpenId Connect (OIDC) JWT tokens. The client application is an Angular single page application that is configured to use the OAuth 2.0 specification for "Browser-Based Apps". An Authorization Code flow with the PKCE extension is used. More information on the specification [here](https://oauth.net/2/browser-based-apps/). 

2. [Angular](https://angular.io/) single page application. This application was was found in the community and is an excellent example of an OAuth 2.0 authenticated client browser-based app. A 3rd party library was used for the OAuth 2.0 and can be found [here](https://github.com/manfredsteyer/angular-oauth2-oidc). 

3. [KrakenD](https://www.krakend.io/) - "KrakenD is a stateless, distributed, high-performance API Gateway that helps you effortlessly adopt microservices". This API gateway framework is written in Go and was simple to configure for this demo. Similar Alternatives are [Kong](https://konghq.com/products/api-gateway-platform) and [Ocelot](https://github.com/ThreeMammals/Ocelot). The JWT token issued by the keycloak service is validated in the API gateway and if invalid, returns an unauthorized response.

4. [Node.js](https://nodejs.org/en/) sample microservices. There are two very minimal node.js microservice examples that simply return "helloworld". 
    * todo: setup simple crud services on these example services to complete this demo

5. [Docker](https://www.docker.com/) is used for this demo to sping up all the services locally with ```docker-compose```.

## Credits

* Angular app starter was found here: https://github.com/jeroenheijmans/sample-angular-oauth2-oidc-with-auth-guards/. An excellent sample and thank you very much.